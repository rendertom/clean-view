![Clean View](/Clean%20View.png)

# Clean View #
Clean View is a script for After Effects to perform multiple actions on Viewer panel.

* Center composition in Viewer
* Set Magnification ratio
* Set Channel and Color Management Settings
* Set Resolution/Down Sample Factor
* Toggle Transparency Grid
* Manage Fast Previews
* Adjust Exposure

### Perform actions on: ###
* All Compositions in the project
* Selected Compositions
* Selected Compositions + Precomps
* Active Composition
* Active Composition + Precomps


### Side Effect: ###
Script has to open compositions to change view settings.
Set **closeCompsWhenFinished** to **false** to leave compositions opened or **true** to close them after script is done running.

### Installation: ###
Copy **Clean View.jsx** to ScriptUI Panels folder:

* **Windows**: Program Files\Adobe\Adobe After Effects <version>\- Support Files\Scripts
* **Mac OS**: Applications/Adobe After Effects <version>/Scripts

Once Installation is finished run the script in After Effects by clicking Window -> **Clean View**

---------
Developed by Tomas Šinkūnas
www.rendertom.com
---------