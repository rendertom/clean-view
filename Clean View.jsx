/**********************************************************************************************
    Clean View.jsx
    Copyright (c) 2017 Tomas Šinkūnas. All rights reserved.
    www.rendertom.com

    Description:
    	Performs multiple actions on Viewer panel.
	
	Side Effect:
		Script has to open compositions to change view settings.
		Set closeCompsWhenFinished to false to leave compositions opened
		or true to close them after script is done running.
**********************************************************************************************/

(function (thisObj) {

	var closeCompsWhenFinished = true;

	scriptBuildUI(thisObj);

	function scriptBuildUI(thisObj) {
		var win = (thisObj instanceof Panel) ? thisObj : new Window("palette", "cleanView", undefined);
		win.spacing = 5;
		win.alignChildren = ["fill", "fill"];

		var prefWidth = 120;

		var checkCenterComposition = win.add("checkbox", undefined, "Center Composition in Viewer");
		checkCenterComposition.onClick = function () {
			grpMagnification.enabled = !this.value;
		};

		var grpMagnification = win.add("group");
		var checkMagnification = grpMagnification.add("checkbox", undefined, "Magnification Ratio");
		checkMagnification.preferredSize.width = prefWidth;
		checkMagnification.onClick = function () {
			checkCenterComposition.enabled = !this.value;
		};
		var etMagnification = grpMagnification.add("edittext", undefined, "100");
		etMagnification.characters = 5;

		var grpChannel = win.add("group");
		var checkChannel = grpChannel.add("checkbox", undefined, "Channel");
		checkChannel.preferredSize.width = prefWidth;
		var channelsObj = getEnumeratedValues(ChannelType);
		var ddChannel = grpChannel.add("dropdownlist", undefined, cleanNames(channelsObj.names, "CHANNEL"));
		ddChannel.alignment = ["fill", "fill"];
		ddChannel.selection = 9;

		var grpResolution = win.add("group");
		var checkResolution = grpResolution.add("checkbox", undefined, "Resolution Factor");
		checkResolution.preferredSize.width = prefWidth;
		var ddResolution = grpResolution.add("dropdownlist", undefined, ["Full", "Half", "Third", "Quarter"]);
		ddResolution.alignment = ["fill", "fill"];
		ddResolution.selection = 0;

		var grpTransparency = win.add("group");
		var checkTransparency = grpTransparency.add("checkbox", undefined, "Transparency Grid");
		checkTransparency.preferredSize.width = prefWidth;
		var grpTransparencyRadio = grpTransparency.add("group");
		var radioTransparencyON = grpTransparencyRadio.add("radiobutton", undefined, "On");
		var radioTransparencyOFF = grpTransparencyRadio.add("radiobutton", undefined, "Off");
		radioTransparencyOFF.value = true;

		var grpPreviews = win.add("group");
		var checkPreviews = grpPreviews.add("checkbox", undefined, "Fast Previews");
		checkPreviews.preferredSize.width = prefWidth;
		var previewsObj = getEnumeratedValues(FastPreviewType);
		var ddPreviews = grpPreviews.add("dropdownlist", undefined, cleanNames(previewsObj.names, "FP"));
		ddPreviews.alignment = ["fill", "fill"];
		ddPreviews.preferredSize.width = 50;
		ddPreviews.selection = 3;

		var grpExposure = win.add("group");
		var checkExposure = grpExposure.add("checkbox", undefined, "Adjust Exposure");
		checkExposure.preferredSize.width = prefWidth;
		var etExposure = grpExposure.add("edittext", undefined, "0");
		etExposure.characters = 5;

		win.add("panel");

		var grpFinish = win.add("group");
		grpFinish.alignChildren = ["fill", "fill"];
		grpFinish.orientation = "column";
		var grpSelectComposition = grpFinish.add("group");
		var ddCompositions = grpSelectComposition.add("dropdownlist", undefined, ["All Compositions", "Selected Compositions", "Active Composition"]);
		ddCompositions.selection = 2;
		ddCompositions.onChange = function () {
			checkPrecomps.enabled = (this.selection.index !== 0);
		};
		var checkPrecomps = grpSelectComposition.add("checkbox", undefined, "Precomps");
		checkPrecomps.helpTip = "Affect deeply nested compositions.";
		checkPrecomps.value = true;

		var btnGo = grpFinish.add("button", undefined, "Do it!");
		btnGo.onClick = function () {
			var compositions = getCompositions(ddCompositions.selection.text, checkPrecomps.value);
			if (!compositions) return;

			for (var i = 0, il = compositions.length; i < il; i++) {
				compositions[i].openInViewer();

				if (checkCenterComposition.value) centerComposition();
				if (checkMagnification.value) setMagnification(parseInt(etMagnification.text) / 100);
				if (checkChannel.value) setChannel(channelsObj.enumValues[ddChannel.selection.index]);
				if (checkResolution.value) setResolution(compositions[i], ddResolution.selection.index);
				if (checkTransparency.value) setTransparency(radioTransparencyON.value);
				if (checkPreviews.value) setFastPreview(previewsObj.enumValues[ddPreviews.selection.index]);
				if (checkExposure.value) setExposure(parseInt(etExposure.text));
			}

			if (closeCompsWhenFinished === true)
				app.executeCommand(4); // 4 is "Close"

			writeLn("cleanView done!");
		};

		win.onResizing = win.onResize = function () {
			this.layout.resize();
		};

		if (win instanceof Window) {
			win.center();
			win.show();
		} else {
			win.layout.layout(true);
			win.layout.resize();
		}
	}

	function centerComposition() {
		var zoomValue = app.activeViewer.views[0].options.zoom;
		app.activeViewer.views[0].options.zoom = zoomValue;
	}

	function setMagnification(value) {
		isRetina() && value /= 2;
		app.activeViewer.views[0].options.zoom = value;
	}

	function setChannel(enumValue) {
		app.activeViewer.views[0].options.channels = enumValue;
	}

	function setResolution(composition, value) {
		var factor = value + 1;
		composition.resolutionFactor = [factor, factor];
	}

	function setTransparency(value) {
		app.activeViewer.views[0].options.checkerboards = value;
	}

	function setFastPreview(enumValue) {
		app.activeViewer.views[0].options.fastPreview = enumValue;
	}

	function setExposure(value) {
		app.activeViewer.views[0].options.exposure = value;
	}


	function getEnumeratedValues(enumeratedObject) {
		var namesArray = [];
		var enumsArray = [];
		for (var property in enumeratedObject) {
			if (enumeratedObject.hasOwnProperty(property)) {
				namesArray.push(property);
				enumsArray.push(enumeratedObject[property]);
			}
		}
		return {
			names: namesArray,
			enumValues: enumsArray
		};
	}

	function cleanNames(namesArray, wordToRemove) {
		var cleanNames = [];
		var newName = "";
		var separator = "_";
		for (var i = 0, il = namesArray.length; i < il; i++) {
			newName = replaceString(namesArray[i], wordToRemove + separator, "");
			newName = replaceString(newName, separator, " ");
			newName = capitalise(newName, " ");
			cleanNames.push(newName);
		}
		return cleanNames;
	}

	function replaceString(string, whatToReplace, replacement) {
		return string.replace(whatToReplace, replacement);
	}

	function capitalise(string, separator) {
		var lowerCaseString = string.toLowerCase();
		var wordsArray = lowerCaseString.split(separator);
		for (var i = 0, il = wordsArray.length; i < il; i++) {
			if (wordsArray[i] === "rgb") wordsArray[i] = "RGB";
			else wordsArray[i] = wordsArray[i].charAt(0).toUpperCase() + wordsArray[i].substr(1);
		}
		return wordsArray.join(separator);
	}

	function isRetina() {
		if (isMac()) {
			var command = "system_profiler SPDisplaysDataType | grep Resolution";
			var result = system.callSystem(command);
			if (result.toLowerCase().match("retina")) {
				return true;
			}
		}

		return false;
	}

	function isMac() {
		return $.os.indexOf("Mac") !== -1;
	}

	function getCompositions(ddSelection, precomps) {
		var compositions = [];
		switch (ddSelection) {
		case "All Compositions":
			compositions = getAllCompositions();
			if (compositions) return compositions;
			else return null;
			break;
		case "Selected Compositions":
			compositions = getSelectedCompositions();
			if (!compositions) return null;
			break;
		case "Active Composition":
			var composition = getActiveComposition();
			if (!composition) return null;
			compositions.push(composition);
			break;
		default:
			return alert("Unassigned dropdownlist value " + ddSelection);
		}

		if (precomps) compositions = getPrecomps(compositions);
		return compositions;

		function getAllCompositions() {
			var compositions = [];
			for (var i = 1, il = app.project.numItems; i <= il; i++) {
				if (app.project.item(i) instanceof CompItem) {
					compositions.push(app.project.item(i));
				}
			}
			if (compositions.length) return compositions;
			else return alert("Project contains no compositions.");
		}

		function getSelectedCompositions() {
			var compositions = [];
			for (var i = 0, il = app.project.selection.length; i < il; i++) {
				if (app.project.selection[i] instanceof CompItem) {
					compositions.push(app.project.selection[i]);
				}
			}
			if (compositions.length > 0) return compositions;
			else return alert("Please select some compositions.");
		}

		function getActiveComposition() {
			var composition = app.project.activeItem;
			if (!composition || !(composition instanceof CompItem))
				return alert("Please select composition first.");

			return composition;
		}

		function getPrecomps(composition) {
			var compsArray = isArray(composition) ? composition : [composition];
			var layer;

			for (var i = 0; i < compsArray.length; i++) {
				for (var j = 1, cl = compsArray[i].numLayers; j <= cl; j++) {
					layer = compsArray[i].layer(j);
					if (layer.source instanceof CompItem && !contains(compsArray, layer.source)) {
						compsArray.push(layer.source);
					}
				}
			}

			return compsArray;

			function contains(array, value) {
				for (var i = 0, il = array.length; i < il; i++) {
					if (array[i] === value) {
						return true;
					}
				}
				return false;
			}

			function isArray(arg) {
				return Object.prototype.toString.call(arg) === "[object Array]";
			}
		}
	}
})(this);